package com.qatestlab.task2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.qatestlab.task2.utils.Properties.getBaseAdminUrl;
import static java.util.concurrent.TimeUnit.SECONDS;

public abstract class BaseScript {

    protected static WebDriver driver;

    protected static WebDriver getDriver() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, SECONDS);
        return driver;
    }

    protected static void login() {
        driver = getDriver();
        driver.get(getBaseAdminUrl());
        WebElement loginField = driver.findElement(By.id("email"));
        loginField.sendKeys("webinar.test@gmail.com");
        WebElement passwordField = driver.findElement(By.id("passwd"));
        passwordField.sendKeys("Xcg7299bnSmMuRLp9ITw");
        WebElement loginButton = driver.findElement(By.cssSelector("button[type='submit']"));
        loginButton.click();
    }

    protected static void tearDown() {
        if(driver != null) {
            driver.quit();
        }
    }
}