package com.qatestlab.task2.tests;

import com.qatestlab.task2.BaseScript;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class CheckMainMenuTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {
        login();
        List<WebElement> webElements = driver.findElements(By.cssSelector(".maintab"));
        List<String> searchContexts = new ArrayList<>();
        webElements.stream().map(e -> e.getText()).forEach(s -> searchContexts.add(s));
        for (String searchContext : searchContexts) {
            WebElement webElement = driver.findElement(By.linkText(searchContext));
            webElement.click();
            System.out.println("the title of page: |" + driver.getTitle() + "| ");
            driver.navigate().refresh();
            System.out.println("refreshing...");
            System.out.println("the title of page: |" + driver.getTitle() + "|");
            System.out.println("");
        }
        tearDown();
    }
}