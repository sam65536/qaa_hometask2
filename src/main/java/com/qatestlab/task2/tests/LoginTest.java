package com.qatestlab.task2.tests;

import com.qatestlab.task2.BaseScript;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {
        login();
        WebElement menuUser = driver.findElement(By.cssSelector("#employee_infos"));
        menuUser.click();
        WebElement logoutButton = driver.findElement(By.cssSelector("#header_logout"));
        logoutButton.click();
        tearDown();
    }
}